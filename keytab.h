/* keytab.h --- Read MIT style Kerberos Credential Cache file.
 * Copyright (C) 2006, 2007  Simon Josefsson
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this file; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA
 *
 */

#ifndef KEYTAB_H
#define KEYTAB_H 1

#include <stdint.h>
#include <string.h>

#define KEYTAB_MAX_COMPONENTS 5
#define KEYTAB_MAX_KEYLEN 32

struct keytab_buffer
{
  uint16_t length;
  char *data;
};

struct keytab_principal
{
  uint32_t name_type;
  uint16_t num_components;
  struct keytab_buffer realm;
  struct keytab_buffer components[KEYTAB_MAX_COMPONENTS];
};

struct keytab_keyblock
{
  uint8_t vno8;
  uint16_t type;
  uint16_t keylen;
  char *keyvalue;
  uint32_t vno;

  char storage[KEYTAB_MAX_KEYLEN];  /* usable by caller for storing
				       keys that keyvalue point to. */
};

struct keytab_entry
{
  int32_t size;
  uint16_t num_components;
  struct keytab_principal name;
  uint32_t timestamp;
  struct keytab_keyblock key;
};

struct keytab
{
  uint16_t file_format_version;
  uint16_t headerlen;
  char *header;
  struct keytab_principal default_principal;
  size_t credentialslen;
  char *credentials;
  uint32_t keytabslen;
  char *keytabs;
};

extern int keytab_parse (const char *data, size_t length, struct keytab *out);

extern int keytab_parse_entry (const char *data, size_t len,
			       struct keytab_entry *out,
			       size_t * n);

#ifdef ENABLE_PRINT
extern void keytab_print (struct keytab *keytab);
extern void keytab_print_principal (struct keytab_principal *princ);
extern void keytab_print_entry (struct keytab_entry *cred);
#endif

#endif /* KEYTAB_H */
