/* keytab.c --- Read MIT style Kerberos hostkey files.
 * Copyright (C) 2006, 2007  Simon Josefsson
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this file; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA
 *
 */

#include "keytab.h"
#include <stdio.h>

/* See keytab.txt for a description of the file format.
 */

static int
get_uint8 (const char **data, size_t * len, uint8_t * i)
{
  const char *p = *data;
  if (*len < 1)
    return -1;
  *i = p[0];
  *data += 1;
  *len -= 1;
  return 0;
}

static int
get_uint16 (const char **data, size_t * len, uint16_t * i)
{
  const char *p = *data;
  if (*len < 2)
    return -1;
  *i = p[0] << 8 | p[1];
  *data += 2;
  *len -= 2;
  return 0;
}

static int
get_uint32 (const char **data, size_t * len, uint32_t * i)
{
  const char *p = *data;
  if (*len < 4)
    return -1;
  *i = ((p[0] << 24) & 0xFF000000)
    | ((p[1] << 16) & 0xFF0000) | ((p[2] << 8) & 0xFF00) | (p[3] & 0xFF);
  *data += 4;
  *len -= 4;
  return 0;
}

static int
parse_principal (const char **data, size_t * len,
		 struct keytab_principal *out)
{
  size_t n;
  int rc;

  rc = get_uint16 (data, len, &out->num_components);
  if (rc < 0)
    return rc;

  if (out->num_components >= KEYTAB_MAX_COMPONENTS)
    return -1;

  rc = get_uint16 (data, len, &out->realm.length);
  if (rc < 0)
    return rc;

  if (*len < out->realm.length)
    return -1;
  out->realm.data = *data;
  *data += out->realm.length;
  *len -= out->realm.length;

  /* Make sure realm will be zero terminated.  This limits the initial
     component length to 2^24 bytes. */
  if (!*len || **(char**)data != '\0')
    return -1;

  for (n = 0; n < out->num_components; n++)
    {
      rc = get_uint16 (data, len, &out->components[n].length);
      if (rc < 0)
	return rc;

      if (*len < out->components[n].length)
	return -1;
      out->components[n].data = *data;
      *data += out->components[n].length;
      *len -= out->components[n].length;

      /* Make sure component is zero terminated.  This limits the name
	 type to values below 2^24.  */
      if (!*len || **(char**)data != '\0')
	return -1;
    }

  rc = get_uint32 (data, len, &out->name_type);
  if (rc < 0)
    return rc;

  return 0;
}

static int
parse_entry (const char **data, size_t * len,
	     struct keytab_entry *out)
{
  struct keytab_principal princ;
  uint32_t num_address;
  uint32_t num_authdata;
  int32_t size;
  int rc;

  rc = get_uint32 (data, len, &size);
  if (rc < 0)
    return rc;

  if (*len < size)
    return -1;

  out->size = size;

  *len -= size;

  rc = parse_principal (data, &size, &out->name);
  if (rc < 0)
    return rc;

  rc = get_uint32 (data, &size, &out->timestamp);
  if (rc < 0)
    return rc;

  rc = get_uint8 (data, &size, &out->key.vno8);
  if (rc < 0)
    return rc;

  rc = get_uint16 (data, &size, &out->key.type);
  if (rc < 0)
    return rc;

  rc = get_uint16 (data, &size, &out->key.keylen);
  if (rc < 0)
    return rc;

  if (size < out->key.keylen)
    return -1;

  out->key.keyvalue = *data;

  *data += out->key.keylen;
  size -= out->key.keylen;

  if (size == 4)
    {
      rc = get_uint32 (data, &size, &out->key.vno);
      if (rc < 0)
	return rc;
    }
  else
    out->key.vno = -1;

  if (size != 0)
    return -1;

  return 0;
}

int
keytab_parse (const char *data, size_t len, struct keytab *out)
{
  size_t pos = 0;
  int rc;
  size_t size;

  rc = get_uint16 (&data, &len, &out->file_format_version);
  if (rc < 0)
    return rc;

  out->keytabs = data;
  out->keytabslen = len;

  /* Validate size integrity of file. */
  do
    {
      rc = get_uint32 (&data, &len, &size);
      if (rc < 0)
	return rc;

      if (len < size)
	return -1;

      data += size;
      len -= size;
    }
  while (len > 0);

  return 0;
}

int
keytab_parse_entry (const char *data, size_t len,
		    struct keytab_entry *out, size_t * n)
{
  size_t savelen = len;
  int rc = parse_entry (&data, &len, out);

  if (rc < 0)
    return rc;

  *n = savelen - len;
  return 0;
}

#if defined (TEST) || defined (ENABLE_PRINT)
void
keytab_print (struct keytab *keytab)
{
  size_t n;

  printf ("file_format_version %04x\n", keytab->file_format_version);
}

void
keytab_print_principal (struct keytab_principal *princ)
{
  size_t n;

  printf ("\tnum_components %04x\n", princ->num_components);
  printf ("\trealmlen %04x\n", princ->realm.length);
  printf ("\trealm %.*s\n", princ->realm.length, princ->realm.data);

  for (n = 0; n < princ->num_components; n++)
    {
      printf ("\t\tcomponentlen %04x\n", princ->components[n].length);
      printf ("\t\tcomponent %.*s\n", princ->components[n].length,
	      princ->components[n].data);
    }
  printf ("\tname_type %04x\n", princ->name_type);
}

void
keytab_print_entry (struct keytab_entry *keytab)
{
  size_t i;
  printf ("\tsize %04x\n", keytab->size);
  printf ("\tnum_components %04x\n", keytab->num_components);
  keytab_print_principal (&keytab->name);
  printf ("\tkey:\n");
  printf ("\t\tvno8 %04x\n", keytab->key.vno8);
  printf ("\t\tkeytype %04x\n", keytab->key.type);
  printf ("\t\tkeylen %04x\n", keytab->key.keylen);
  printf ("\t\tkey value: ");
  for (i = 0; i < keytab->key.keylen; i++)
    printf ("%02x", ((char *) keytab->key.keyvalue)[i] & 0xFF);
  printf ("\n");
  if (keytab->key.vno != -1)
    printf ("\t\tvno %04x\n", keytab->key.vno);
}
#endif

#ifdef TEST
int
main (int argc, char *argv[])
{
  char buf[10240];
  size_t len;
  FILE *fh;
  int rc;
  struct keytab keytab;
  struct keytab_entry entry;
  size_t i = 0;

  if (argc <= 1)
    {
      printf ("Usage: %s <krb5keytab-file>\n", argv[0]);
      return 1;
    }

  fh = fopen (argv[1], "rb");
  if (!fh)
    {
      puts ("Error: cannot open file");
      return 1;
    }

  len = fread (buf, 1, sizeof (buf), fh);

  if (len >= sizeof (buf))
    {
      puts ("Error: file too large");
      return 1;
    }

  rc = keytab_parse (buf, len, &keytab);
  if (rc < 0)
    {
      puts ("Error: syntax error");
      return 1;
    }

  keytab_print (&keytab);

  while (keytab.keytabslen)
    {
      size_t n;

      rc = keytab_parse_entry (keytab.keytabs,
			       keytab.keytabslen, &entry, &n);
      if (rc < 0)
	{
	  printf ("Error: cannot parse credential %d\n", i);
	  return rc;
	}

      printf ("\nCredential %d:\n", i++);

      keytab_print_entry (&entry);

      keytab.keytabs += n;
      keytab.keytabslen -= n;
    }

  if (fclose (fh))
    {
      puts ("Error: cannot close file");
      return 1;
    }

  return 0;
}
#endif
